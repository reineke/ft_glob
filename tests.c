/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tests.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 17:13:49 by mvladymy          #+#    #+#             */
/*   Updated: 2019/06/30 19:01:53 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <glob.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/types.h>
#include <limits.h>

#include "ft_glob.h"

#include <locale.h>
#include <stdio.h>
#include <string.h>

void	glob_test(char *pattern, size_t offset, int flags)
{
	glob_t	gl;
	int		ret;
	size_t	i;

	gl.gl_offs = offset;
	ret = ft_glob(pattern, flags, 0, &gl);
	printf("offset: %zu path number: %zu\n", gl.gl_offs, gl.gl_pathc);
	i = 0;
	while (i < gl.gl_offs + gl.gl_pathc)
		printf("\t%s\n", gl.gl_pathv[i++]);
	printf("return code: %d\n", ret);
	ft_globfree(&gl);
}

void	range_test_match(char ch, char *pattern, bool assert)
{
	size_t	a = 0;
	bool	find;

	find = range_match(ch, pattern, &a);
	printf("%lc %s %zu (`%c' in \"%s\")\n", ((assert == find) ? L'✅' : L'❌'),
											((find) ? "true " : "false"),
											a, ch, pattern);
}

void	glob_match_test(char *path, char *pattern, bool assert)
{
	bool	match;

	match = dir_match(path, pattern, 0, 0);
	printf("%lc %s (\"%s\" and \"%s\")\n", ((assert == match) ? L'✅' : L'❌'),
											((match) ? "true " : "false"),
											path, pattern);

}

int		main(void)
{
	setlocale(LC_ALL, "");

	/*range_test_match('b', "[b]", true);
	range_test_match('b', "[ab]", true);
	range_test_match('b', "[bc]", true);
	range_test_match('b', "[abc]", true);
	range_test_match('b', "[\\b]", true);
	range_test_match('b', "[\\bc]", true);
	range_test_match('b', "[a\\b]", true);
	range_test_match('b', "[a\\bc]", true);
	range_test_match('b', "[a-b]", true);
	range_test_match('b', "[b-c]", true);
	range_test_match('b', "[a-c]", true);
	range_test_match('b', "[a-\\c]", true);
	range_test_match('b', "[\\a-c]", true);
	range_test_match('b', "[]", false);
	range_test_match('[', "[[]", true);
	range_test_match(']', "[]]", true);
	range_test_match(']', "[]a]", true);
	range_test_match('b', "[b", false);
	range_test_match(']', "[a\\]]", true);
	range_test_match('-', "[a-]]", true);
	range_test_match('b', "[!abc]", false);
	range_test_match('d', "[!abc]", true);
	range_test_match('!', "[ab!c]", true);
	range_test_match('!', "[abc!]", true);
	range_test_match('!', "[!abc]", true);
	range_test_match('!', "[\\!abc]", true);
	range_test_match('!', "[!!]", false);
	range_test_match('#', "[!-A]", true);

	printf("\n");*/

	glob_match_test("foo", "foo", true);
	glob_match_test("foo/bar", "foo/", true);
	glob_match_test("foo", "foo/bar", true);
	glob_match_test("foo/", "foo", true);
	glob_match_test("foo", "foo/", true);
	glob_match_test("foo", "fooo", false);
	glob_match_test("foo", "fo?", true);
	glob_match_test("foo[]", "foo[]", true);
	glob_match_test("foo", "fo[a-z]", true);
	glob_match_test("foo", "f[a-z]o", true);
	glob_match_test("foo", "fo[a-c]", false);
	glob_match_test("fo[a]", "fo[a]", false);
	glob_match_test("fo[a]", "fo\\[a]", true);
	glob_match_test("foo[bar", "foo[bar", true);
	glob_match_test("foo", "fo\\o", true);
	glob_match_test(".git", ".git", true);
	glob_match_test(".git", "?git", false);
	glob_match_test(".git", "[.]git", false);
	glob_match_test("..git", "..git", true);
	glob_match_test("..git", ".?git", true);
	glob_match_test("..git", ".[.]git", true);
	glob_match_test("..", "..", true);
	glob_match_test("..", "??", false);
	glob_match_test("..", ".?", true);
	glob_match_test("..", "?.", false);
	glob_match_test("..", ".[.]", true);
	glob_match_test("..", "[.][.]", false);
	glob_match_test("..", "[.].", false);
	glob_match_test(".", ".", true);
	glob_match_test(".", "?", false);
	glob_match_test(".", "[.]", false);
	glob_match_test(".git", ".*t", true);	
	glob_match_test(".git", ".*i", false);	
	glob_match_test(".git", ".*", true);
	glob_match_test(".git", ".git*", true);
	glob_match_test(".git", "*", false);
	glob_match_test(".git", "*git", false);
	glob_match_test(".git", ".*git", true);
	glob_match_test("git", "*", true);
	glob_match_test("git", "*t", true);
	glob_match_test("git", "*i*", true);
	glob_match_test("sobaka.exe", "*.*", true);
	glob_match_test("sobaka.exe", "*u*", false);
	glob_match_test("sobaka.exe", "sobaka.*", true);
	glob_match_test("sobaka.exe", "*.exe", true);
	glob_match_test("..", "*", false);
	glob_match_test("..", ".*", true);
	glob_match_test("..", "*.", false);
	glob_match_test(".", ".*", true);
	glob_match_test(".", "*", false);
	glob_match_test(".", "*.", false);
	glob_match_test("sobaka", "*[a-z]aka", true);
	glob_match_test("sobaka", "*b[a-z]ka", true);
	glob_match_test("sobaka", "s*[a-z]ka", true);
	glob_match_test("sobaka", "*b[a-z]*ka", true);
	glob_match_test("sobaka", "s[a-z]*ka", true);
	glob_match_test("sobaka", "*ba[A-Z]a", false);
	glob_match_test("sobaka", "so*u[a-z]a", false);

	//printf("\n");

	//glob_test("[.]???", 0, 0);

#ifdef __APPLE__
	system("leaks a.out");
#endif
}
